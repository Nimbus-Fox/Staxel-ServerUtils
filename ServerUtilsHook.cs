﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Rev2.Classes;
using NimbusFox.ServerUtils.Classes.BlobRecords;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Player;
using Staxel.Tiles;

namespace NimbusFox.ServerUtils {
    internal class ServerUtilsHook : IModHookV4 {

        internal static ServerUtilsHook Instance;

        internal BlobDatabase _storageDatabase;
        internal KitsuneCore.V1.KitsuneCore KsCore;
        private bool _selfCheck;

        static ServerUtilsHook() {
            ModHelper.CheckModIsInstalled("ServerUtils", "Kitsune Core");
        }

        public ServerUtilsHook() {
            Instance = this;

            KsCore = new KitsuneCore.V1.KitsuneCore("NimbusFox", "ServerUtils");
            if (Helpers.IsServer()) {
                _storageDatabase = new BlobDatabase(KsCore.SaveDirectory.ObtainFileStream("data.db", FileMode.OpenOrCreate),
                    true, KsCore.LogError);
            }
        }

        /// <inheritdoc />
        public void Dispose() {
            _storageDatabase.Dispose();
        }

        /// <inheritdoc />
        public void GameContextInitializeInit() { }

        /// <inheritdoc />
        public void GameContextInitializeBefore() { }

        /// <inheritdoc />
        public void GameContextInitializeAfter() { }

        /// <inheritdoc />
        public void GameContextDeinitialize() { }

        /// <inheritdoc />
        public void GameContextReloadBefore() { }

        /// <inheritdoc />
        public void GameContextReloadAfter() { }

        /// <inheritdoc />
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }

        /// <inheritdoc />
        public void UniverseUpdateAfter() { }

        /// <inheritdoc />
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            if (Helpers.IsServer()) {
                if (entity.Logic is PlayerEntityLogic logic) {
                    if (!_selfCheck) {
                        _selfCheck = true;
                        if (GameContext.ModdingController.CanPlaceTile(entity, location, tile, accessFlags)) {
                            var record = _storageDatabase.CreateRecord<TileHistoryRecord>();

                            record.Location = location;
                            record.Tile = tile.Configuration;
                            record.User = logic;
                        }

                        _selfCheck = false;
                    }
                }
            }
            return true;
        }

        /// <inheritdoc />
        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            if (Helpers.IsServer()) {
                if (entity.Logic is PlayerEntityLogic logic) {
                    if (!_selfCheck) {
                        _selfCheck = true;
                        if (GameContext.ModdingController.CanReplaceTile(entity, location, tile, accessFlags)) {
                            var record = _storageDatabase.CreateRecord<TileHistoryRecord>();

                            record.Location = location;
                            record.Tile = tile.Configuration;
                            record.User = logic;
                        }

                        _selfCheck = false;
                    }
                }
            }
            return true;
        }

        /// <inheritdoc />
        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            if (Helpers.IsServer()) {
                if (entity.Logic is PlayerEntityLogic logic) {
                    if (!_selfCheck) {
                        _selfCheck = true;
                        if (GameContext.ModdingController.CanRemoveTile(entity, location, accessFlags)) {
                            if (KsCore.WorldManager.Universe.ReadTile(location, accessFlags, ChunkFetchKind.LivingWorld,
                                entity.Id, out var tile)) {
                                var record = _storageDatabase.CreateRecord<TileHistoryRecord>();

                                record.Location = location;
                                record.Tile = tile.Configuration;
                                record.User = logic;
                            }
                        }

                        _selfCheck = false;
                    }
                }
            }
            return true;
        }

        /// <inheritdoc />
        public void ClientContextInitializeInit() { }

        /// <inheritdoc />
        public void ClientContextInitializeBefore() { }

        /// <inheritdoc />
        public void ClientContextInitializeAfter() { }

        /// <inheritdoc />
        public void ClientContextDeinitialize() { }

        /// <inheritdoc />
        public void ClientContextReloadBefore() { }

        /// <inheritdoc />
        public void ClientContextReloadAfter() { }

        /// <inheritdoc />
        public void CleanupOldSession() { }

        /// <inheritdoc />
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        /// <inheritdoc />
        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}

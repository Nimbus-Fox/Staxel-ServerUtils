﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1.Classes;
using NimbusFox.KitsuneCore.V1.Classes.BlobRecord;
using Plukit.Base;
using Staxel;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Tiles;

namespace NimbusFox.ServerUtils.Classes.BlobRecords {
    internal class TileHistoryRecord : BaseRecord {
        /// <inheritdoc />
        public TileHistoryRecord(BlobDatabase database, Blob blob, Guid id) : base(database, blob, id) {
            Location = Vector3I.Zero;
            Edited = DateTime.Now;
        }

        public Vector3I Location {
            get => _blob.FetchBlob("location").GetVector3I();
            set {
                _blob.FetchBlob("location").SetVector3I(value);
                Save();
            }
        }

        public DateTime Edited {
            get => new DateTime(_blob.GetLong("edited", 0));
            set {
                _blob.SetLong("edited", value.Ticks);
                Save();
            }
        }

        public PlayerEntityLogic User {
            get => (PlayerEntityLogic) ServerUtilsHook.Instance.KsCore.UserManager.GetPlayerEntityByUid(_blob.GetString("user", "")).Logic;
            set {
                _blob.SetString("user", value.Uid());
                Save();
            }
        }

        public TileConfiguration Tile {
            get => GameContext.TileDatabase.GetTileConfiguration(_blob.GetString("tile", ""));
            set {
                _blob.SetString("tile", value.Code);
                Save();
            }
        }
    }
}
